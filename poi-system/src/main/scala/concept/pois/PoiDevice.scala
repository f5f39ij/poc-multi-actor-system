/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package concept.pois

import java.time.LocalDateTime
import java.util.UUID

import akka.actor.{ActorLogging, Props}
import akka.persistence.PersistentActor
import concept.common.SafeShutdown
import concept.pois.commands.PaymentTransactionRequest
import concept.pois.events.{PaymentTransactionFailed, PaymentTransactionProcessed, PoiEvent}

class PoiDevice(poiId: String) extends PersistentActor with ActorLogging {

  private var transactionsProcessed = 0
  private var transactionRejected = 0

  override def receiveRecover: Receive = {
    case _ => log.info(s"Receive recover...............")
  }

  override def receiveCommand: Receive = {
    case Tuple2(saleSystem, payment@PaymentTransactionRequest(transactionId, operator, destinationPoiId, paymentData)) =>
      if (destinationPoiId != poiId) {
        val failure = PaymentTransactionFailed(transactionId,
          operator,
          destinationPoiId,
          paymentData,
          s"PoiDevice with id ${poiId}, at ${self.path.address}, can not process on behalf ${destinationPoiId}")
        persist(failure)(trackProcessingCount)
        context.parent ! Tuple2(saleSystem, failure)
      } else {
        log.info(s"PoiDevice with id ${poiId}, at ${self.path.address}, processed successfully payment -> ${payment}")
        val success = PaymentTransactionProcessed(transactionId,
          operator,
          destinationPoiId,
          paymentData,
          UUID.randomUUID().toString,
          LocalDateTime.now()
        )
        persist(success)(trackProcessingCount)
        context.parent ! Tuple2(saleSystem, success)
      }

    case SafeShutdown =>
      context.stop(self)
    case _ =>
      log.warning(s"C'mon send me some proper messages.")
      self ! SafeShutdown
  }

  override def persistenceId: String = poiId

  private def trackProcessingCount: PoiEvent => Unit = {
    case _: PaymentTransactionProcessed => transactionsProcessed += 1
    case _: PaymentTransactionFailed => transactionRejected += 1
  }
}

object PoiDevice {
  def props(poiId: String): Props = Props(new PoiDevice(poiId))
}
