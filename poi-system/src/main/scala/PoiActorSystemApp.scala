/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import concept.pois.PoiManager

object PoiActorSystemApp extends App {
  val config = ConfigFactory.load()
  val actorSystem = ActorSystem.create(config.getString("akka.actor.system"), config)
  println(actorSystem)

  val poiManagerId = "poi-manager"
  println(s"Creating SaleSystem ${poiManagerId}")
  val poiManager = actorSystem.actorOf(PoiManager.props, poiManagerId)
  println(s"Poi manager created at -> ${poiManager} | ${poiManager.path}")

}
