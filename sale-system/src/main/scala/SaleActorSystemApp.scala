/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import concept.sales.SalePaymentManager

object SaleActorSystemApp extends App {
  val config = ConfigFactory.load()
  val actorSystem = ActorSystem.create(config.getString("akka.actor.system"), config)
  println(actorSystem.toString)

  val saleSystemManagerId = "sale-system-manager"
  println(s"Creating SaleSystem ${saleSystemManagerId}")
  val saleSystemManager = actorSystem.actorOf(SalePaymentManager.props, saleSystemManagerId)
  println(s"Poi manager created at -> ${saleSystemManager} | ${saleSystemManager.path}")
}
