/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package concept.pois.events

import java.util.UUID

import cloud.yantra.patterns.es.{Event, EventId}

trait PoiEvent extends Event {

  override def id: EventId = EventId(uuid.toString)

  def uuid: UUID = UUID.randomUUID()

}
