/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package concept.sales.commands

import java.util.UUID

case class PrintProcessingRequestCount() extends SaleCommand {

  override val uuid: UUID = UUID.randomUUID()

}
