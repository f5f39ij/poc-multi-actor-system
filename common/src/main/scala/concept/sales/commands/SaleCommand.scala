/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package concept.sales.commands

import java.util.UUID

import cloud.yantra.patterns.cqrs.{Command, CommandId}

trait SaleCommand extends Command {

  override def id: CommandId = CommandId(uuid.toString)

  def uuid: UUID
}
