/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package concept.sales

import concept.sales.events.{SaleTransactionIdAlreadyUsed, SaleTransactionInitiated, SaleTransactionProcessedSuccessfully, SaleTransactionProcessingFailed}
import play.api.libs.json._

object JsonSerializer {

  implicit val saleTransactionInitiatedWrites = Json.writes[SaleTransactionInitiated]
  implicit val saleTransactionProcessedSuccessfullyWrites = Json.writes[SaleTransactionProcessedSuccessfully]
  implicit val saleTransactionProcessingFailedWrites = Json.writes[SaleTransactionProcessingFailed]
  implicit val saleTransactionIdAlreadyUsedWrites = Json.writes[SaleTransactionIdAlreadyUsed]
}
