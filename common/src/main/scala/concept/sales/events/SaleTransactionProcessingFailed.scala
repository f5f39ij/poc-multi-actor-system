/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package concept.sales.events

import java.util.UUID

import cloud.yantra.patterns.es.EntityId

case class SaleTransactionProcessingFailed(transactionId: Int,
                                           operator: String,
                                           poiId: String,
                                           paymentData: String,
                                           reason: String) extends SaleTransactionEvent {

  override def entityId: Option[EntityId] = Some(EntityId(transactionId.toString))

  override val uuid: UUID = UUID.randomUUID()

}
