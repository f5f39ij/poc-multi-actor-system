/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package concept.sales.events

import java.time.LocalDateTime
import java.util.UUID

import cloud.yantra.patterns.es.EntityId

case class SaleTransactionProcessedSuccessfully(transactionId: Int,
                                                operator: String,
                                                poiId: String,
                                                paymentData: String,
                                                poiTransactionId: String,
                                                time: LocalDateTime) extends SaleTransactionEvent {

  override def entityId: Option[EntityId] = Some(EntityId(transactionId.toString))

  override val uuid: UUID = UUID.randomUUID()

}
