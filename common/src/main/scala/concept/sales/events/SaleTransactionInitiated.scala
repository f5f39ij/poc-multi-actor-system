/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package concept.sales.events

import java.time.LocalDateTime
import java.util.UUID

import cloud.yantra.patterns.es.EntityId

case class SaleTransactionInitiated(transactionId: Int,
                                    operator: String,
                                    paymentData: String,
                                    time: LocalDateTime) extends SaleTransactionEvent {

  override def entityId: Option[EntityId] = Some(EntityId(transactionId.toString))

  override val uuid: UUID = UUID.randomUUID()

}
