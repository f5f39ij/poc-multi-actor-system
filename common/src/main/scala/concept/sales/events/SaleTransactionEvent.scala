/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package concept.sales.events

import java.util.UUID

import cloud.yantra.patterns.es.{Event, EventId}

trait SaleTransactionEvent extends Event {

  override def id: EventId = EventId(uuid.toString)

  def uuid: UUID
}
