/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package concept.operators

import akka.actor.{Actor, ActorLogging, ActorRef, Props}

class OperatorManager() extends Actor with ActorLogging {

  override def receive: Receive = {
    case OperatorManager.Commands.Subscribe(operator, observer) =>
      fetchOperator(operator) ! OperatorActor.Commands.Subscribe(observer)

    case OperatorManager.Commands.Unsubscribe(operator, observer) =>
      fetchOperator(operator) ! OperatorActor.Commands.Unsubscribe(observer)
  }

  private def fetchOperator(operator: String): ActorRef = context.child(operator) match {
    case Some(child) =>
      child
    case None =>
      context.actorOf(OperatorActor.props(operator), operator)
  }
}

object OperatorManager {
  def props(): Props = Props(new OperatorManager())

  object Commands {

    case class Subscribe(operator: String, observer: ActorRef)

    case class Unsubscribe(operator: String, observer: ActorRef)

  }

}
