/*
 * Copyright (C) 2017-2018 Yantra Cloud Ltd. <https://yantra.cloud>
 */

package concept.operators

import akka.actor.{Actor, ActorLogging, ActorRef, Props, Terminated}
import akka.cluster.pubsub.DistributedPubSub
import akka.cluster.pubsub.DistributedPubSubMediator.{Subscribe, Unsubscribe}
import concept.sales.JsonSerializer._
import concept.sales.events.{SaleTransactionIdAlreadyUsed, SaleTransactionInitiated, SaleTransactionProcessedSuccessfully, SaleTransactionProcessingFailed}
import play.api.libs.json._

class OperatorActor(operator: String) extends Actor with ActorLogging {

  val mediator = DistributedPubSub(context.system).mediator
  val subscribeTopic = s"responses/${operator}"
  var observers: List[ActorRef] = Nil

  override def preStart(): Unit = {
    log.info(s"########################### Mediator: ${mediator.path}, Subscriber: ${self.path}")
    mediator ! Subscribe(subscribeTopic, self)
    super.preStart()
  }

  override def receive: Receive = {
    case msg: SaleTransactionProcessedSuccessfully =>
      val event = Json.toJson(msg)
      log.info(event.toString())
      publish(event)

    case msg: SaleTransactionProcessingFailed =>
      val event = Json.toJson(msg)
      log.info(event.toString())
      publish(event)

    case msg: SaleTransactionInitiated =>
      val event = Json.toJson(msg)
      log.info(event.toString())
      publish(event)

    case msg: SaleTransactionIdAlreadyUsed =>
      val event = Json.toJson(msg)
      log.info(event.toString())
      publish(event)

    case Terminated(observer) =>
      log.info("Actor [{}] is terminated", observer.path)
      observers = observers.filter(observer == _)

    case OperatorActor.Commands.Subscribe(observer) =>
      observers = observers :+ observer
      context.watch(observer)
      ()

    case unknown =>
      println(unknown)
      val event = s"Unknown: ${unknown}"
      log.warning(event)
  }

  override def postStop(): Unit = {
    log.info(s"###########################${Unsubscribe(subscribeTopic, self)}")
    mediator ! Unsubscribe(subscribeTopic, self)
    super.postStop()
  }

  private def publish(event: JsValue): Unit = observers.foreach(_ ! event)
}

object OperatorActor {
  def props(operator: String): Props = Props(new OperatorActor(operator))

  object Commands {

    case class Subscribe(observer: ActorRef)

    case class Unsubscribe(observer: ActorRef)

  }

}
